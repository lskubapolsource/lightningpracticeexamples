({
	myAction : function(component, event, helper) {
	},

	handleEvent : function(component, event, helper) {
		console.log("handleEvent: " + component.get("v.componentName"));
	},

	handleEventCapture : function(component, event, helper) {
		console.log("handleEventCapture: " + component.get("v.componentName"));
	},

	handleEventBubble : function(component, event, helper) {
		console.log("handleEventBubble: " + component.get("v.componentName"));
	},
})