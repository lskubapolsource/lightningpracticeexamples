({
	handleClick : function(component, event, helper) {

		console.log("handleClick");


		// var compEvent = component.getEvent("sampleEvent");
		// compEvent.fire();

		var appEvent = $A.get("e.c:LE_TestEvent");
		appEvent.fire();
	},


	handleEvent : function(component, event, helper) {
		console.log("handleEvent: " + component.get("v.componentName"));
	},

	handleEventCapture : function(component, event, helper) {
		console.log("handleEventCapture: " + component.get("v.componentName"));
	},

	handleEventBubble : function(component, event, helper) {
		console.log("handleEventBubble: " + component.get("v.componentName"));
	},
})